# Contributing to CASlab IoT

### Branches and Merge Requests

There are two protected branches **master** and **develop** that only maintainers can push to. Additionally, only maintainers can merge to master but everyone can merge to develop.

To contribute, the developers open merge requests with target=**develop**. When they finish they can merge their branch and open a merge request for source=**develop** to target=**master**.

This way the **develop** branch will be used for new changes and the **master** branch will only have the latest stable changes.

### Versioning

The lerna tool will be used from **master** to create new CASlab IoT versions.

For example `npx lerna version patch` creates a new patch version and create a tagged commit from which we can build and deploy the new version. and deploy the new version.
