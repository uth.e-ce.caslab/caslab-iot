# Monorepo

The CASlab IoT repository uses [lerna](https://lernajs.io/) to multiple packages.

Packages:

- [@caslab-iot/common](../packages/common): A pure library that contains JS/TS files for code/definitions sharing between packages
- [@caslab-iot/api-server](../packages/api-server): The api web server microservice that is built using typescript and [Loopback 4](loopback.io)
- [@caslab-iot/web-client](../packages/web-client): The front-end that will be used by the users to interract with CASlab IoT. It is built using [ReactJS](https://reactjs.org/).
