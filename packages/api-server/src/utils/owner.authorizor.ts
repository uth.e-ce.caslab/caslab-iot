import {
  AuthorizationContext,
  AuthorizationDecision,
  AuthorizationMetadata,
} from '@loopback/authorization';
import {DefaultCrudRepository, Entity} from '@loopback/repository';
import {securityId, UserProfile} from '@loopback/security';

// dummy model definition just to make sure that id,userId are defined properties
export interface OnwerBasedModel extends Entity {
  id: string;
  userId?: string;
}

/**
 * Owner authorizor factory that takes the name of the target repository and
 * whether to user 'id' or 'userId' as owner's id property key.
 * @param repoName
 * @param useId
 */
export function ownerAuthorizorFactory<
  RepoType extends DefaultCrudRepository<OnwerBasedModel, string>
>(repoName: string, useId?: boolean) {
  const repoPath = `repositories.${repoName}`;
  const finalFields = useId ? {id: true} : {userId: true};
  /**
   * Voter that authorizes based on owner (userId relation). If not owner but
   * context contains roles then returns ABSTAIN instead to allow role authorizer
   * to decide.
   * @param authorizationCtx
   * @param metadata
   */
  return async function ownerAuthorizor(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ): Promise<AuthorizationDecision> {
    // expects that first argument is the resourceId.
    const resourceId =
      authorizationCtx.invocationContext.args.length >= 1
        ? authorizationCtx.invocationContext.args[0]
        : undefined;
    if (!authorizationCtx.principals.length || !resourceId) {
      return AuthorizationDecision.ABSTAIN;
    }
    const repo: RepoType = await authorizationCtx.invocationContext.get(
      repoPath,
    );
    const resource: OnwerBasedModel = await repo.findById(resourceId, {
      fields: finalFields,
    });
    if (!resource) return AuthorizationDecision.ABSTAIN;
    // if userId is defined, then must be stringified, because it's ObjectId when used as foreignKey.
    const resourceOwnerId = useId ? resource.id : resource.userId?.toString();
    if (!resourceOwnerId) return AuthorizationDecision.ABSTAIN;

    const userProfile: UserProfile = authorizationCtx.principals[0];
    // note that for JWT auth userProfile.securityId is the userId

    // uses id.toString to make sure that the result is
    const isOwner = userProfile[securityId] === resourceOwnerId.toString();
    if (!isOwner) {
      // if has allowed roles then role authorizer can decide
      return metadata.allowedRoles?.length
        ? AuthorizationDecision.ABSTAIN
        : AuthorizationDecision.DENY;
    }

    return AuthorizationDecision.ALLOW;
  };
}
