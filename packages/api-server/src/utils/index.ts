import {OperationObject} from '@loopback/openapi-v3';
import _ from 'lodash';
export * from './owner.authorizor';

export const mergeWithOASJWT = (spec: OperationObject): OperationObject =>
  _.merge(
    {
      responses: {
        '401': {
          description: 'Authorization information is missing or invalid.',
        },
      },
      security: [{jwt: []}],
    },
    spec,
  );
