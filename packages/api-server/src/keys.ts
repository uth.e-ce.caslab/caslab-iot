import {BindingKey} from '@loopback/core';
import {JWTService} from './services';

export namespace JWTServiceConstants {
  export const TOKEN_SECRET_DEFAULT = 'jwtsecret';
  export const TOKEN_EXPIRES_IN_DEFAULT = '3600'; // sec, 1h
}

export namespace JWTServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expires.in.seconds',
  );
  export const TOKEN_SERVICE = BindingKey.create<JWTService>(
    'services.authentication.jwt.tokenservice',
  );
}

export namespace PasswordHasherConstants {
  export const ROUNDS_DEFAULT = 10;
}

export namespace PasswordHasherBindings {
  export const ROUNDS = BindingKey.create<number>('services.hasher.rounds');
}
