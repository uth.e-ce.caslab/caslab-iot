import {
  AuthorizationContext,
  AuthorizationDecision,
  AuthorizationMetadata,
  Authorizer,
} from '@loopback/authorization';
import {Provider} from '@loopback/core';

export class RolesAuthorizationProvider implements Provider<Authorizer> {
  constructor() {}

  /**
   * Returns the authorize method
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    // if no allowed roles then we are unable to decide
    if (!metadata.allowedRoles?.length) return AuthorizationDecision.ABSTAIN;

    const {roles = []} = authorizationCtx.principals[0] || {};
    const {allowedRoles} = metadata;
    return allowedRoles.some(allowedRole => roles.includes(allowedRole))
      ? AuthorizationDecision.ALLOW
      : AuthorizationDecision.DENY;
  }
}
