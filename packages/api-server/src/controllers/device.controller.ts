import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {Device, User} from '../models';
import {DeviceRepository} from '../repositories';
import {mergeWithOASJWT, ownerAuthorizorFactory} from '../utils';

@authenticate('jwt')
export class DeviceController {
  constructor(
    @repository(DeviceRepository)
    public deviceRepository: DeviceRepository,
  ) {}

  @post(
    '/devices',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Device model instance',
          content: {'application/json': {schema: getModelSchemaRef(Device)}},
        },
      },
    }),
  )
  async create(
    @inject(SecurityBindings.USER)
    userProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {
            title: 'NewDevice',
            exclude: ['id'],
          }),
        },
      },
    })
    device: Device,
  ): Promise<Device> {
    let forceOnwerDevice;
    if (!userProfile.roles.includes('admin')) {
      forceOnwerDevice = {
        ...device,
        userId: userProfile[securityId],
      };
    }
    return this.deviceRepository.create(forceOnwerDevice ?? device);
  }

  @get(
    '/devices/count',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Device model count',
          content: {'application/json': {schema: CountSchema}},
        },
      },
    }),
  )
  async count(@param.where(Device) where?: Where<Device>): Promise<Count> {
    return this.deviceRepository.count(where);
  }

  @get(
    '/devices',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Array of Device model instances',
          content: {
            'application/json': {
              schema: {
                type: 'array',
                items: getModelSchemaRef(Device, {includeRelations: false}),
              },
            },
          },
        },
      },
    }),
  )
  async find(
    @inject(SecurityBindings.USER)
    userProfile: UserProfile,
    @param.filter(Device) filter?: Filter<Device>,
  ): Promise<Device[]> {
    let ownerFilter;
    if (!userProfile.roles.includes('admin')) {
      const ownerWhere = {userId: userProfile[securityId]};
      const where: Where<Device> = filter?.where
        ? {and: [ownerWhere, filter.where]}
        : ownerWhere;
      ownerFilter = {
        ...filter,
        where,
      };
    }
    return this.deviceRepository.find(ownerFilter ?? filter);
  }

  @authorize({
    allowedRoles: ['admin'],
  })
  @patch(
    '/devices',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Device PATCH success count',
          content: {'application/json': {schema: CountSchema}},
        },
      },
    }),
  )
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {partial: true}),
        },
      },
    })
    device: Device,
    @param.where(Device) where?: Where<Device>,
  ): Promise<Count> {
    return this.deviceRepository.updateAll(device, where);
  }

  @authorize({
    allowedRoles: ['admin'],
    voters: [ownerAuthorizorFactory(DeviceRepository.name)],
  })
  @get(
    '/devices/{id}',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'User model instance',
          content: {'application/json': {schema: getModelSchemaRef(User)}},
        },
      },
    }),
  )
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Device, {exclude: 'where'})
    filter?: FilterExcludingWhere<Device>,
  ): Promise<Device> {
    return this.deviceRepository.findById(id, filter);
  }

  @authorize({
    allowedRoles: ['admin'],
    voters: [ownerAuthorizorFactory(DeviceRepository.name)],
  })
  @patch(
    '/devices/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Device PATCH success',
        },
      },
    }),
  )
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Device, {partial: true}),
        },
      },
    })
    device: Device,
  ): Promise<void> {
    await this.deviceRepository.updateById(id, device);
  }

  @authorize({
    allowedRoles: ['admin'],
    voters: [ownerAuthorizorFactory(DeviceRepository.name)],
  })
  @put(
    '/devices/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Device PUT success',
        },
      },
    }),
  )
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() device: Device,
  ): Promise<void> {
    await this.deviceRepository.replaceById(id, device);
  }

  @authorize({
    allowedRoles: ['admin'],
    voters: [ownerAuthorizorFactory(DeviceRepository.name)],
  })
  @del(
    '/devices/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Device DELETE success',
        },
      },
    }),
  )
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.deviceRepository.deleteById(id);
  }
}
