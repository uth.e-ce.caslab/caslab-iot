import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {service} from '@loopback/core';
import {get, post, requestBody} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {JWTServiceBindings} from '../keys';
import {Credentials} from '../repositories/user.repository';
import {JWTService, MyUserService} from '../services';
import {mergeWithOASJWT} from '../utils';

// TODO: remove this workarround
const CredentialsSchema = {
  type: 'object',
  required: ['username', 'password'],
  properties: {
    username: {
      type: 'string',
    },
    password: {
      type: 'string',
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};

export class AuthController {
  constructor(
    @inject(JWTServiceBindings.TOKEN_SERVICE)
    public jwtService: JWTService,
    @service(MyUserService)
    public userService: MyUserService,
  ) {}

  @authenticate('jwt')
  @get(
    '/auth/whoami',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: "User's profile name",
          content: {
            'application/json': {
              schema: {
                type: 'object',
                properties: {
                  name: {
                    type: 'string',
                  },
                },
              },
            },
          },
        },
      },
    }),
  )
  whoAmI(
    @inject(SecurityBindings.USER)
    userProfile: UserProfile,
  ): {name: string} {
    const {name} = userProfile;
    return {
      name: name ? name : 'unknown',
    };
  }

  @post('/auth/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                id: {
                  type: 'string',
                },
                token: {
                  type: 'string',
                },
                roles: {
                  type: 'array',
                  items: {
                    type: 'string',
                  },
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{id: string; token: string; roles: string[]}> {
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);
    return {id: userProfile.id, token, roles: userProfile.roles};
  }
}
