import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {Measurement} from '../models';
import {MeasurementRepository} from '../repositories';
import {mergeWithOASJWT, ownerAuthorizorFactory} from '../utils';

@authenticate('jwt')
@authorize({
  allowedRoles: ['admin'],
  voters: [ownerAuthorizorFactory(MeasurementRepository.name)],
})
export class MeasurementController {
  constructor(
    @repository(MeasurementRepository)
    public measurementRepository: MeasurementRepository,
  ) {}

  @post(
    '/measurements',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Measurement model instance',
          content: {
            'application/json': {schema: getModelSchemaRef(Measurement)},
          },
        },
      },
    }),
  )
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Measurement, {
            title: 'NewMeasurement',
            exclude: ['id'],
          }),
        },
      },
    })
    measurement: Omit<Measurement, 'id'>,
  ): Promise<Measurement> {
    return this.measurementRepository.create(measurement);
  }

  @get(
    '/measurements/count',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Measurement model count',
          content: {'application/json': {schema: CountSchema}},
        },
      },
    }),
  )
  async count(
    @param.where(Measurement) where?: Where<Measurement>,
  ): Promise<Count> {
    return this.measurementRepository.count(where);
  }

  @get(
    '/measurements',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Array of Measurement model instances',
          content: {
            'application/json': {
              schema: {
                type: 'array',
                items: getModelSchemaRef(Measurement, {
                  includeRelations: false,
                }),
              },
            },
          },
        },
      },
    }),
  )
  async find(
    @param.filter(Measurement) filter?: Filter<Measurement>,
  ): Promise<Measurement[]> {
    return this.measurementRepository.find(filter);
  }

  @patch(
    '/measurements',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Measurement PATCH success count',
          content: {'application/json': {schema: CountSchema}},
        },
      },
    }),
  )
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Measurement, {partial: true}),
        },
      },
    })
    measurement: Measurement,
    @param.where(Measurement) where?: Where<Measurement>,
  ): Promise<Count> {
    return this.measurementRepository.updateAll(measurement, where);
  }

  @get(
    '/measurements/{id}',
    mergeWithOASJWT({
      responses: {
        '200': {
          description: 'Measurement model instance',
          content: {
            'application/json': {
              schema: getModelSchemaRef(Measurement, {includeRelations: false}),
            },
          },
        },
      },
    }),
  )
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Measurement, {exclude: 'where'})
    filter?: FilterExcludingWhere<Measurement>,
  ): Promise<Measurement> {
    return this.measurementRepository.findById(id, filter);
  }

  @patch(
    '/measurements/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Measurement PATCH success',
        },
      },
    }),
  )
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Measurement, {partial: true}),
        },
      },
    })
    measurement: Measurement,
  ): Promise<void> {
    await this.measurementRepository.updateById(id, measurement);
  }

  @put(
    '/measurements/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Measurement PUT success',
        },
      },
    }),
  )
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() measurement: Measurement,
  ): Promise<void> {
    await this.measurementRepository.replaceById(id, measurement);
  }

  @del(
    '/measurements/{id}',
    mergeWithOASJWT({
      responses: {
        '204': {
          description: 'Measurement DELETE success',
        },
      },
    }),
  )
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.measurementRepository.deleteById(id);
  }
}
