import {get, ResponseObject} from '@loopback/rest';

const STATUS_RESPONSE: ResponseObject = {
  description: 'Status Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'StatusResponse',
        properties: {
          status: {type: 'string'},
          date: {type: 'string'},
        },
      },
    },
  },
};

export class StatusController {
  @get('/status', {
    responses: {
      '200': STATUS_RESPONSE,
    },
  })
  status(): object {
    return {
      date: new Date(),
      status: 'OK',
    };
  }
}
