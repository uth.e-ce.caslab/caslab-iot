import {
  asAuthStrategy,
  AuthenticationStrategy,
  TokenService,
} from '@loopback/authentication';
import {bind, inject} from '@loopback/context';
import {
  asSpecEnhancer,
  mergeSecuritySchemeToSpec,
  OASEnhancer,
  OpenApiSpec,
} from '@loopback/openapi-v3';
import {HttpErrors, Request} from '@loopback/rest';
import {UserProfile} from '@loopback/security';
import {JWTServiceBindings} from '../keys';

@bind(asAuthStrategy, asSpecEnhancer)
export class JWTAuthenticationStrategy
  implements AuthenticationStrategy, OASEnhancer {
  name = 'jwt';

  constructor(
    @inject(JWTServiceBindings.TOKEN_SERVICE)
    public tokenService: TokenService,
  ) {}

  async authenticate(request: Request): Promise<UserProfile | undefined> {
    const token: string = this.extractCredentials(request);
    const userProfile: UserProfile = await this.tokenService.verifyToken(token);
    return userProfile;
  }

  extractCredentials(request: Request): string {
    const {authorization} = request.headers;
    if (!authorization) {
      throw new HttpErrors.Unauthorized('Authorization header not found');
    }
    const parts = authorization.split(' ');
    const [type, token] = parts;
    if (type !== 'Bearer') {
      throw new HttpErrors.Unauthorized(
        'Authorization header is not of type "Bearer"',
      );
    }
    if (parts.length !== 2) {
      throw new HttpErrors.Unauthorized('Invalid Bearer token');
    }
    return token;
  }

  modifySpec(spec: OpenApiSpec): OpenApiSpec {
    return mergeSecuritySchemeToSpec(spec, this.name, {
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
    });
  }
}
