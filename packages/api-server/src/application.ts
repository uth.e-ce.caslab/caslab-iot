import {
  AuthenticationComponent,
  registerAuthenticationStrategy,
} from '@loopback/authentication';
import {
  AuthorizationComponent,
  AuthorizationDecision,
  AuthorizationTags,
} from '@loopback/authorization';
import {BootMixin} from '@loopback/boot';
import {Application, ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {
  RestApplication,
  RestBindings,
  RestComponent,
  RestServer,
} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import {
  JWTServiceBindings,
  JWTServiceConstants,
  PasswordHasherBindings,
  PasswordHasherConstants,
} from './keys';
import {MQTTServer} from './mqtt.server';
import {RolesAuthorizationProvider} from './providers';
import {MySequence} from './sequence';
import {JWTService} from './services';
import {JWTAuthenticationStrategy} from './strategies';

export {ApplicationConfig};

export class ApiServerApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(Application)),
) {
  public restServer: RestServer;
  public mqttServer: MQTTServer;
  public restApp: RestApplication;
  constructor(config: ApplicationConfig = {}) {
    super(config);
    this.projectRoot = __dirname;
    this.bootOptions = {
      controllers: {
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
    this._setupRestServer();
    this._setupMqttServer();
  }

  /**
   * Setups the rest server.
   */
  private _setupRestServer(): void {
    this.component(RestComponent);
    this.component(AuthenticationComponent);
    const authorizationBinding = this.component(AuthorizationComponent);
    this.configure(authorizationBinding.key).to({
      precedence: AuthorizationDecision.ALLOW, // if one voter says so allow
      defaultDecision: AuthorizationDecision.DENY, // if all abstain then deny
    });
    this.bind(JWTServiceBindings.TOKEN_SECRET).to(
      JWTServiceConstants.TOKEN_SECRET_DEFAULT, // TODO: get from ENV
    );
    this.bind(JWTServiceBindings.TOKEN_EXPIRES_IN).to(
      JWTServiceConstants.TOKEN_EXPIRES_IN_DEFAULT, // TODO: get from ENV
    );
    this.bind(JWTServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    this.bind(PasswordHasherBindings.ROUNDS).to(
      PasswordHasherConstants.ROUNDS_DEFAULT,
    );
    this.bind('authorizationProviders.rolesAuthorizationProvider')
      .toProvider(RolesAuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER);
    registerAuthenticationStrategy(this, JWTAuthenticationStrategy);
    this.bind(RestBindings.REQUEST_BODY_PARSER_OPTIONS).to({
      validation: {
        $data: true,
        ajvKeywords: true,
        ajvErrors: true,
      },
    });
    this.server(RestServer);
    this.restServer = this.getSync<RestServer>('servers.RestServer');
    this.restServer.sequence(MySequence);
  }

  /**
   * Setups the mqtt server
   */
  private _setupMqttServer(): void {
    this.server(MQTTServer);
    this.bind('mqttServer.port').to(1883);
    this.mqttServer = this.getSync<MQTTServer>('servers.MQTTServer');
  }
}
