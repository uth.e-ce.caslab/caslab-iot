import {lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {repository} from '@loopback/repository';
import {UserRepository} from '../repositories';

@lifeCycleObserver('')
export class AdminUserCreatorObserver implements LifeCycleObserver {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  /**
   * Checks for the existence of the default admin and if entry does not
   * exist it creates it.
   */
  async start(): Promise<void> {
    const defaultAdmin = {
      username: 'admin',
      password: '1234', // TODO: later replace with ENV variable
      email: 'admin@caslab-iot-email.com',
      roles: ['admin'],
    };
    const adminExists = await this.userRepository.findOne({
      where: {username: defaultAdmin.username},
    });
    if (!adminExists) {
      await this.userRepository.create(defaultAdmin);
    }
  }

  async stop(): Promise<void> {
    // nothing to do here
  }
}
