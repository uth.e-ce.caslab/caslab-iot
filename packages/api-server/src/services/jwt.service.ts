import {TokenService} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {bind, BindingScope} from '@loopback/core';
import {HttpErrors} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {promisify} from 'util';
import {JWTServiceBindings} from '../keys';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

@bind({scope: BindingScope.TRANSIENT})
export class JWTService implements TokenService {
  constructor(
    @inject(JWTServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
    @inject(JWTServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
  ) {}

  async verifyToken(token: string): Promise<UserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(
        'Error verifying token: "token" is null',
      );
    }
    let userProfile: UserProfile;
    try {
      const decodedToken = await verifyAsync(token, this.jwtSecret);
      userProfile = {
        [securityId]: decodedToken.id,
        name: decodedToken.name,
        id: decodedToken.id,
        roles: decodedToken.roles,
      };
    } catch (err) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token: ${err.message}`,
      );
    }
    return userProfile;
  }

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token: userProfile is null',
      );
    }
    const tokenInfo = {
      id: userProfile[securityId],
      name: userProfile.name,
      roles: userProfile.roles,
    };
    let token: string;
    try {
      token = await signAsync(tokenInfo, this.jwtSecret, {
        expiresIn: Number(this.jwtExpiresIn),
      });
    } catch (err) {
      throw new HttpErrors.Unauthorized(`Error encoding token: ${err}`);
    }

    return token;
  }
}
