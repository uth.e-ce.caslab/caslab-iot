import {bind, BindingScope, inject} from '@loopback/core';
import {compare, genSalt, hash} from 'bcrypt';
import {PasswordHasherBindings} from '../keys';

@bind({scope: BindingScope.TRANSIENT})
export class PasswordHasherService {
  constructor(
    @inject(PasswordHasherBindings.ROUNDS)
    private readonly rounds: number,
  ) {}

  async hashPassword(pass: string): Promise<string> {
    const salt = await genSalt(this.rounds);
    return hash(pass, salt);
  }

  async comparePassword(passA: string, passB: string): Promise<boolean> {
    return compare(passA, passB);
  }
}
