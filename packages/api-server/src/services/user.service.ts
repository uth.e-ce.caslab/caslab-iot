import {UserService} from '@loopback/authentication';
import {bind, BindingScope, service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {User} from '../models/user.model';
import {Credentials, UserRepository} from '../repositories/user.repository';
import {PasswordHasherService} from './password-hasher.service';

@bind({scope: BindingScope.TRANSIENT})
export class MyUserService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @service(PasswordHasherService)
    public passwordHasher: PasswordHasherService,
  ) {}

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const invalidCredentialsError = 'Invalid credentials';
    const foundUser = await this.userRepository.findOne({
      where: {username: credentials.username},
    });
    if (!foundUser) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const passwordMatched = await this.passwordHasher.comparePassword(
      credentials.password,
      foundUser.password,
    );
    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    return foundUser;
  }

  convertToUserProfile(user: User): UserProfile {
    if (!user?.id) {
      throw new HttpErrors.Unauthorized('Invalid user');
    }
    let name = '';
    if (user.firstName) name = user.firstName;
    if (user.lastName) {
      name = user.firstName ? `${name} ${user.lastName}` : user.lastName;
    }
    // if both first name and last name are undefined then use username
    if (!name) name = user.username;
    return {
      [securityId]: user.id,
      name,
      id: user.id,
      roles: user.roles ?? [],
    };
  }
}
