import {
  Application,
  Context,
  CoreBindings,
  inject,
  Server,
} from '@loopback/core';
import aedes from 'aedes';
import {once} from 'events';
import net from 'net';

export class MQTTServer extends Context implements Server {
  private _listening = false;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _mqttServer: any;
  private _mqttPort: number;
  private _netServer: net.Server;

  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE) public app: Application,
    @inject('mqttServer.port') public port: number,
  ) {
    super(app);
    this._mqttPort = port;
    this._mqttServer = aedes();
    this._netServer = net.createServer(this._mqttServer.handle);
  }
  get listening() {
    return this._listening;
  }
  async start(): Promise<void> {
    this._netServer.listen(this._mqttPort);
    this._listening = true;
    await once(this._netServer, 'listening');
  }
  async stop(): Promise<void> {
    this._netServer.close();
    this._listening = false;
    await once(this._netServer, 'close');
  }
}
