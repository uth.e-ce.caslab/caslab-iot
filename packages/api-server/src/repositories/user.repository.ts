import {inject, service} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {MongoDataSource} from '../datasources';
import {User, UserRelations} from '../models';
import {PasswordHasherService} from '../services/password-hasher.service';

export type Credentials = {
  username: string;
  password: string;
};

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
    @service(PasswordHasherService)
    public passwordHasher: PasswordHasherService,
  ) {
    super(User, dataSource);
  }

  /**
   * Hook that checks for the uniqueness of user's unique properties (username,
   * email)
   * @param ctx The before save hook context
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async beforeSaveHookUniqueProps(ctx: any) {
    const {id, username = '', email = ''} = ctx.instance || ctx.data || {};
    if (username || email) {
      const userAlreadyExists = await this.findOne({
        where: {or: [{username}, {email}]},
      });
      if (userAlreadyExists && (!id || (id && userAlreadyExists.id !== id))) {
        if (userAlreadyExists.username === username) {
          throw new HttpErrors.Conflict(
            `Username '${username}' already exists.`,
          );
        } else if (userAlreadyExists.email === email) {
          throw new HttpErrors.Conflict(`Email '${email}' already exists.`);
        } else {
          // this should never happen but it's left here just for failsafe
          throw new HttpErrors.Conflict('Similar user already exists');
        }
      }
    }
  }

  /**
   * Hook that replaces the persist data password with a new hashed password.
   * hashed password.
   * @param ctx The persist hook context
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async persistHookHashPassword(ctx: any) {
    const {password} = ctx.data;
    if (password) {
      ctx.data.password = await this.passwordHasher.hashPassword(password);
    }
  }

  definePersistedModel(entityClass: typeof User) {
    const modelClass = super.definePersistedModel(entityClass);
    modelClass.observe(
      'before save',
      this.beforeSaveHookUniqueProps.bind(this),
    );
    modelClass.observe('persist', this.persistHookHashPassword.bind(this));
    return modelClass;
  }
}
