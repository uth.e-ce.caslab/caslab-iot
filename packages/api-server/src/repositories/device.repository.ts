import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {MongoDataSource} from '../datasources';
import {Device, DeviceRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class DeviceRepository extends DefaultCrudRepository<
  Device,
  typeof Device.prototype.id,
  DeviceRelations
> {
  public readonly user: BelongsToAccessor<User, typeof Device.prototype.id>;
  public readonly border: BelongsToAccessor<Device, typeof Device.prototype.id>;

  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('DeviceRepository')
    protected deviceRepositoryGetter: Getter<DeviceRepository>,
  ) {
    super(Device, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
    this.border = this.createBelongsToAccessorFor(
      'border',
      deviceRepositoryGetter,
    );
    this.registerInclusionResolver('border', this.border.inclusionResolver);
  }
}
