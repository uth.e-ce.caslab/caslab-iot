import {Model, model, property} from '@loopback/repository';

@model()
export class DeviceMeta extends Model {
  @property({
    type: 'date',
  })
  lastSeen?: Date;

  @property({
    type: 'string',
    jsonSchema: {
      format: 'ipv4',
    },
  })
  ipv4?: string;

  @property({
    type: 'string',
    jsonSchema: {
      format: 'ipv6',
    },
  })
  ipv6?: string;

  constructor(data?: Partial<DeviceMeta>) {
    super(data);
  }
}
