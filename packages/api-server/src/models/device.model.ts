import {DeviceTypes} from '@caslab-iot/common';
import {belongsTo, Entity, model, property} from '@loopback/repository';
import {DeviceMeta} from './device-meta.model';
import {User, UserWithRelations} from './user.model';

@model({})
export class Device extends Entity {
  @property({
    // TODO: currently API is restricted from setting id but repository can set
    // it, so find a way to force id generation.
    type: 'string',
    id: true,
    generated: false,
    defaultFn: 'uuidv4',
  })
  id: string;

  @property({
    type: 'string',
    default: '',
  })
  name?: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isBorder: boolean;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: Object.values(DeviceTypes),
    },
  })
  type: string;

  @property({
    type: 'boolean',
    default: false,
  })
  status: boolean;

  @property({
    default: {},
  })
  meta?: DeviceMeta;

  @belongsTo(() => User)
  userId?: string;

  @belongsTo(() => Device)
  borderId?: string;

  constructor(data?: Partial<Device>) {
    super(data);
  }
}

export interface DeviceRelations {
  user?: UserWithRelations;
  border?: DeviceWithRelations;
}

export type DeviceWithRelations = Device & DeviceRelations;
