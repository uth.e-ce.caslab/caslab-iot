import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    hiddenProperties: ['password'],
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    index: true,
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    index: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    default: '',
  })
  firstName: string;

  @property({
    type: 'string',
    default: '',
  })
  lastName: string;

  @property({
    type: 'boolean',
    default: false,
  })
  emailVerified?: boolean;

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  roles?: string[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
