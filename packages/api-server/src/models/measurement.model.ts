import {belongsTo, Entity, model, property} from '@loopback/repository';
import {User, UserWithRelations} from './user.model';

@model()
export class Measurement extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'date',
    required: true,
  })
  timestamp: string;

  @property({
    type: 'number',
    required: true,
  })
  source: number;

  @property({
    type: 'string',
    required: true,
  })
  sourceId: string;

  @property({
    type: 'object',
    default: {},
  })
  meta?: object;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<Measurement>) {
    super(data);
  }
}

export interface MeasurementRelations {
  user?: UserWithRelations;
}

export type MeasurementWithRelations = Measurement & MeasurementRelations;
