import {DeviceTypes} from '@caslab-iot/common';
import {Filter} from '@loopback/repository';
import {Client, expect, supertest} from '@loopback/testlab';
import sinon from 'sinon';
import {ApiServerApplication} from '../..';
import {Device, User} from '../../models';
import {
  applyToken,
  checkUnauth,
  clearDevices,
  clearUsers,
  givenDevices,
  givenDevicesForUser,
  givenToken,
  givenUsers,
  setupApplication,
} from '../helpers';

describe('Testing Device', () => {
  const sandbox = sinon.createSandbox();
  let app: ApiServerApplication;
  let client: Client;

  let user: User;
  let another: User;
  let admin: User;
  let userToken: string;
  let adminToken: string;

  before('setup', async () => {
    sandbox.useFakeTimers({toFake: ['Date']});
    ({app, client} = await setupApplication());
    await clearUsers(app);
    await clearDevices(app);
    const createdUsers = await givenUsers(app);
    user = createdUsers.user;
    admin = createdUsers.admin;
    another = createdUsers.another;
    userToken = await givenToken(app, user);
    adminToken = await givenToken(app, admin);
  });

  after('clean', async () => {
    await app.stop();
    await clearUsers(app);
    sandbox.restore();
  });

  describe('Testing "create" endpoint', () => {
    checkUnauth(createReq);
    beforeEach(async () => {
      return clearDevices(app);
    });
    after(async () => {
      return clearDevices(app);
    });
    it('should create device for user', async () => {
      const devData = {
        name: 'demo',
        type: DeviceTypes.LINUX_X86,
        isBorder: true,
      };
      const res = await createReq(devData, userToken).expect(200);
      expect(res.body).to.containEql({
        ...devData,
        userId: user.id.toString(), // retrieved from token
      });
    });
    it('should create device for user and force userId', async () => {
      const devData = {
        name: 'demo',
        type: DeviceTypes.LINUX_X86,
        isBorder: true,
        userId: another.id.toString(),
      };
      const res = await createReq(devData, userToken).expect(200);
      expect(res.body).to.containEql({
        ...devData,
        userId: user.id.toString(), // should be forced
      });
    });
    it('should create device without user when admin', async () => {
      const devData = {
        name: 'demo',
        type: DeviceTypes.LINUX_X86,
        isBorder: true,
      };
      const res = await createReq(devData, adminToken).expect(200);
      expect(res.body)
        .to.containEql({
          ...devData,
        })
        .to.not.have.property('userId');
    });
    it('should create device for another when admin', async () => {
      const devData = {
        name: 'demo',
        type: DeviceTypes.LINUX_X86,
        isBorder: true,
        userId: another.id.toString(),
      };
      const res = await createReq(devData, adminToken).expect(200);
      expect(res.body).to.containEql({
        ...devData,
      });
    });

    function createReq(data?: Partial<Device>, token?: string): supertest.Test {
      const req = client.post('/devices').send(data);
      return applyToken(req, token);
    }
  });
  describe('Testing "get-devices" endpoint', () => {
    let userDevices: Device[];
    let adminDevices: Device[];
    let anotherUserDevices: Device[];
    let noUserDevices: Device[];
    let allDevices: Device[];
    beforeEach(async () => {
      await clearDevices(app);
      userDevices = await givenDevicesForUser(app, user, 2);
      adminDevices = await givenDevicesForUser(app, admin, 2);
      anotherUserDevices = await givenDevicesForUser(app, another, 2);
      noUserDevices = await givenDevices(app, 2);
      allDevices = [
        ...userDevices,
        ...adminDevices,
        ...anotherUserDevices,
        ...noUserDevices,
      ];
    });
    after(async () => {
      return clearDevices(app);
    });
    checkUnauth(getReq);
    it('should return only user devices', async () => {
      const res = await getReq({}, userToken).expect(200);
      expect(res.body).to.be.an.Array().to.have.lengthOf(userDevices.length);
      expect(res.body).to.containDeep(userDevices);
    });
    it('should return only user devices even when filtering for no user devices', async () => {
      const res = await getReq(
        {where: {userId: {exists: false}}},
        userToken,
      ).expect(200);
      expect(res.body).to.be.an.Array().to.have.lengthOf(0);
    });
    it('should return only user devices even when filtering for another', async () => {
      const res = await getReq(
        {where: {userId: another.id.toString()}},
        userToken,
      ).expect(200);
      expect(res.body).to.be.an.Array().to.have.lengthOf(0);
    });
    it('should return all devices when admin and no filter', async () => {
      const res = await getReq({}, adminToken).expect(200);
      expect(res.body).to.be.an.Array().to.have.lengthOf(allDevices.length);
      expect(res.body).to.containDeep(allDevices);
    });
    it('should return user devices when admin and filter for user', async () => {
      const res = await getReq(
        {where: {userId: another.id.toString()}},
        adminToken,
      ).expect(200);
      expect(res.body)
        .to.be.an.Array()
        .to.have.lengthOf(anotherUserDevices.length);
      expect(res.body).to.containDeep(anotherUserDevices);
    });
    function getReq(
      filter: Filter<Device> = {},
      token?: string,
    ): supertest.Test {
      const req = client
        .get('/devices')
        .query({filter: JSON.stringify(filter)});
      return applyToken(req, token);
    }
  });
});
