import {Client, expect, supertest} from '@loopback/testlab';
import sinon from 'sinon';
import {ApiServerApplication} from '../..';
import {User} from '../../models';
import {
  checkUnauth,
  clearUsers,
  givenAdminAndUser,
  givenToken,
  setupApplication,
} from '../helpers';

describe('Testing Auth', () => {
  const sandbox = sinon.createSandbox();
  let app: ApiServerApplication;
  let client: Client;

  let user: User;
  let admin: User;
  let userToken: string;
  let adminToken: string;

  before('setup', async () => {
    sandbox.useFakeTimers({toFake: ['Date']});
    ({app, client} = await setupApplication());
    await clearUsers(app);
    const createdUsers = await givenAdminAndUser(app);
    user = createdUsers.user;
    admin = createdUsers.admin;
    userToken = await givenToken(app, user);
    adminToken = await givenToken(app, admin);
  });

  after('clean', async () => {
    await app.stop();
    await clearUsers(app);
    sandbox.restore();
  });

  describe('Testing "login" endpoint', () => {
    it('should reject login when wrong paramas', async () => {
      const res = await loginReq().expect(400);
      expect(res.body).to.containEql({
        error: {
          statusCode: 400,
          name: 'BadRequestError',
          message: 'Request body is required',
          code: 'MISSING_REQUIRED_PARAMETER',
        },
      });
    });
    it('should reject login when wrong credentials', async () => {
      const res = await loginReq({
        username: 'dummy',
        password: 'dummy`',
      }).expect(401);
      expect(res.body).to.containEql({
        error: {
          statusCode: 401,
          name: 'UnauthorizedError',
          message: 'Invalid credentials',
        },
      });
    });
    it('should login user', async () => {
      const res = await loginReq({
        username: user.username,
        password: user.password,
      }).expect(200);
      expect(res.body).to.containEql({
        id: user.id.toString(),
        token: userToken,
        roles: user.roles,
      });
    });
    it('should login admin', async () => {
      const res = await loginReq({
        username: admin.username,
        password: admin.password,
      }).expect(200);
      expect(res.body).to.containEql({
        id: admin.id.toString(),
        token: adminToken,
        roles: admin.roles,
      });
    });

    function loginReq(data?: object) {
      return client.post('/auth/login').send(data);
    }
  });

  describe('Testing "whoami" endpoint', () => {
    checkUnauth(whoamiReq);
    it('should reject when authorization is not bearer', async () => {
      const res = await whoamiReq('token', 'Type').expect(401);
      expect(res.body).to.containEql({
        error: {
          statusCode: 401,
          name: 'UnauthorizedError',
          message: 'Authorization header is not of type "Bearer"',
        },
      });
    });
    it('should reject when authorization is not valid bearer token', async () => {
      const res = await whoamiReq('invalid token with extra spaces').expect(
        401,
      );
      expect(res.body).to.containEql({
        error: {
          statusCode: 401,
          name: 'UnauthorizedError',
          message: 'Invalid Bearer token',
        },
      });
    });
    it('should reject when authorization is malformed bearer token', async () => {
      const res = await whoamiReq('malformed').expect(401);
      expect(res.body).to.containEql({
        error: {
          statusCode: 401,
          name: 'UnauthorizedError',
          message: 'Error verifying token: jwt malformed',
        },
      });
    });
    it("should return user's name", async () => {
      const res = await whoamiReq(userToken).expect(200);
      // user has first name and last name specified, so expect full name
      expect(res.body).to.containEql({
        name: `${user.firstName} ${user.lastName}`,
      });
    });
    it("should return admin's name", async () => {
      const res = await whoamiReq(adminToken).expect(200);
      // admin doesn't have first name and last name specified, so username is expected
      expect(res.body).to.containEql({
        name: admin.username,
      });
    });

    function whoamiReq(token?: string, type = 'Bearer'): supertest.Test {
      let req = client.get('/auth/whoami');
      if (token) req = req.set('Authorization', `${type} ${token}`);
      return req;
    }
  });
});
