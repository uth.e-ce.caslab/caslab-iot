import {Client, expect} from '@loopback/testlab';
import sinon from 'sinon';
import {ApiServerApplication} from '../..';
import {setupApplication} from '../helpers';

describe('Testing Status', () => {
  let app: ApiServerApplication;
  let client: Client;
  const sandbox = sinon.createSandbox();

  before('setup', async () => {
    ({app, client} = await setupApplication());
    sandbox.useFakeTimers();
  });
  after(async () => {
    await app.stop();
    sandbox.restore();
  });
  it('should return status', async () => {
    const res = await client.get('/status').expect(200);
    expect(res.body).to.containEql({
      date: new Date().toISOString(),
      status: 'OK',
    });
  });
});
