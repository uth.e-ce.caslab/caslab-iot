import {expect, supertest} from '@loopback/testlab';
import {ApiServerApplication} from '../../application';
import {JWTServiceBindings} from '../../keys';
import {User} from '../../models';
import {MyUserService} from '../../services';

export async function givenToken(app: ApiServerApplication, user: User) {
  const jwtService = await app.get(JWTServiceBindings.TOKEN_SERVICE);
  const userService = await app.get<MyUserService>('services.MyUserService');
  const profile = userService.convertToUserProfile(user);
  return jwtService.generateToken(profile);
}

export function applyToken(
  req: supertest.Test,
  token?: string,
  type = 'Bearer',
): supertest.Test {
  let finalReq = req;
  if (token) finalReq = req.set('Authorization', `${type} ${token}`);
  return finalReq;
}

export type ReqGen = () => supertest.Test;

export function checkUnauth(gen: ReqGen) {
  it('should reject when not authenticated', async () => {
    const res = await gen().expect(401);
    expect(res.body).to.containEql({
      error: {
        statusCode: 401,
        name: 'UnauthorizedError',
        message: 'Authorization header not found',
      },
    });
  });
}
