import {DeviceTypes} from '@caslab-iot/common';
import {ApiServerApplication} from '../../application';
import {Device, User} from '../../models';
import {
  DeviceRepository,
  MeasurementRepository,
  UserRepository,
} from '../../repositories';

/* Users Helpers */
async function createUser(
  userData: Partial<User> = {},
  app: ApiServerApplication,
): Promise<User> {
  const repo = await app.getRepository(UserRepository);
  return repo.create({
    username: 'unspecified',
    password: 'unspecified',
    email: 'unspecified@caslab-iot-test.gr',
    ...userData,
  });
}

export async function clearUsers(app: ApiServerApplication) {
  const repo = await app.getRepository(UserRepository);
  return repo.deleteAll();
}

export async function givenUser(
  app: ApiServerApplication,
  userData: Partial<User> = {},
): Promise<User> {
  return createUser(
    {
      username: 'testuser',
      email: 'user@caslab-iot-test.gr',
      firstName: 'First',
      lastName: 'Last',
      ...userData,
    },
    app,
  );
}

export async function giveAnotherUser(
  app: ApiServerApplication,
): Promise<User> {
  return createUser(
    {
      username: 'testanother',
      email: 'another@caslab-iot-test.gr',
    },
    app,
  );
}

export async function givenAdmin(app: ApiServerApplication): Promise<User> {
  return createUser(
    {
      username: 'testadmin',
      email: 'admin@caslab-iot-test.gr',
      roles: ['admin'],
    },
    app,
  );
}

export async function givenAdminAndUser(
  app: ApiServerApplication,
): Promise<{user: User; admin: User}> {
  const user = await givenUser(app, {});
  const admin = await givenAdmin(app);
  return {user, admin};
}

export async function givenUsers(
  app: ApiServerApplication,
): Promise<{user: User; another: User; admin: User}> {
  const user = await givenUser(app, {});
  const another = await giveAnotherUser(app);
  const admin = await givenAdmin(app);
  return {user, another, admin};
}

/* Devices Helpers */
async function createDevice(
  devData: Partial<Device> = {},
  app: ApiServerApplication,
): Promise<Device> {
  const repo = await app.getRepository(DeviceRepository);
  return repo.create({
    name: 'unspecified',
    type: DeviceTypes.LINUX_X86,
    isBorder: true,
    ...devData,
  });
}

export async function clearDevices(app: ApiServerApplication) {
  const repo = await app.getRepository(DeviceRepository);
  return repo.deleteAll();
}

export async function givenDevicesForUser(
  app: ApiServerApplication,
  user?: User,
  num = 1,
  devData?: Partial<Device>,
): Promise<Device[]> {
  const devs = [];
  const prefix = user ? `${user.id}_` : '';
  for (let i = 0; i < num; i++) {
    const finalData: Partial<Device> = {
      id: `${prefix}DEV_${i}`,
      ...devData,
    };
    if (user) finalData.userId = user.id;
    devs.push(await createDevice(finalData, app));
  }
  return devs;
}

export async function givenDevices(
  app: ApiServerApplication,
  num = 1,
  devData?: Partial<Device>,
): Promise<Device[]> {
  return givenDevicesForUser(app, undefined, num, devData);
}

/* Measurement Helpers */
export async function clearMeasurements(app: ApiServerApplication) {
  const repo = await app.getRepository(MeasurementRepository);
  return repo.deleteAll();
}

/* Generic Helpers */
export async function clearDB(app: ApiServerApplication) {
  return Promise.all([
    clearDevices(app),
    clearUsers(app),
    clearMeasurements(app),
  ]);
}
