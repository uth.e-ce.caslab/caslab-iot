import {
  Client,
  createRestAppClient,
  givenHttpServerConfig,
} from '@loopback/testlab';
import {ApiServerApplication} from '../..';

export function givenApplication(): ApiServerApplication {
  const restConfig = givenHttpServerConfig({});
  const app = new ApiServerApplication({
    rest: restConfig,
  });
  return app;
}

export async function setupApplication(): Promise<AppWithClient> {
  const app = givenApplication();
  await app.boot();
  await app.start();
  const client = createRestAppClient(app);
  return {app, client};
}

export interface AppWithClient {
  app: ApiServerApplication;
  client: Client;
}
