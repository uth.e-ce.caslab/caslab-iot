// supported border device types
export enum BorderTypes {
  LINUX_X86 = 'Linux x86',
  RASPBERRY_PI = 'Raspberry Pi',
}

// supported edge device types
export enum EdgeTypes {
  STM32_EXAMPLE = 'STM32 Example Type', // dummy type for now
}

// list of all supported device types
export const DeviceTypes = {
  ...BorderTypes,
  ...EdgeTypes,
};

export type DeviceTypes = typeof DeviceTypes;

export function shortId(id: string, len = 8): string {
  return id.substring(0, len);
}
