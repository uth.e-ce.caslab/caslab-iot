import {connectRouter} from 'connected-react-router';
import {combineReducers} from 'redux';
import auth from './state/auth';
import devices from './state/devices';
import generic from './state/generic';
import notifications from './state/notifications';
import users from './state/users';

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    generic,
    auth,
    users,
    devices,
    notifications,
  });
export default createRootReducer;
