import {createSlice} from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'notifications',
  initialState: {
    isPopUpHidden: true,
    popUpMessage: '',
    popUpStatus: 'unknown',
  },
  reducers: {
    privateShowPopUp: (state, {payload}) => {
      state.isPopUpHidden = false;
      state.popUpMessage = payload.message;
      state.popUpStatus = payload.status;
    },
    hidePopUp: state => {
      state.isPopUpHidden = true;
      state.popUpMessage = '';
      state.popUpStatus = 'unknown';
    },
  },
});

export const {
  actions: {privateShowPopUp, hidePopUp},
} = slice;

export const showPopUp = payload => (dispatch, getState) => {
  const {isPopUpHidden} = getState().notifications;
  if (!isPopUpHidden) dispatch(hidePopUp());
  return dispatch(privateShowPopUp(payload));
};

export default slice.reducer;
