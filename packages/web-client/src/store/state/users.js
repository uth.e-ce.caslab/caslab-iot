import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import {api} from '../../utils';

const usersAdapter = createEntityAdapter({});

export const fetchUsers = createAsyncThunk(
  'users/get',
  async (unused, {getState, requestId}) => {
    const {loading, currentRequestId} = getState().users;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    const {data} = await api.get('users');
    return {users: data};
  },
);

export const slice = createSlice({
  name: 'users',
  initialState: {
    loading: 'idle',
    currentRequestId: undefined,
    ...usersAdapter.getInitialState(),
  },
  extraReducers: {
    [fetchUsers.pending]: (state, {meta}) => {
      if (state.loading === 'idle') {
        state.loading = 'pending';
        state.currentRequestId = meta.requestId;
      }
    },
    [fetchUsers.fulfilled]: (state, {payload, meta}) => {
      if (
        state.loading === 'pending' &&
        state.currentRequestId === meta.requestId
      ) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        usersAdapter.setAll(state, payload.users);
      }
    },
    [fetchUsers.rejected]: (state, {meta}) => {
      if (
        state.loading === 'pending' &&
        state.currentRequestId === meta.requestId
      ) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
      }
    },
  },
});

export default slice.reducer;
