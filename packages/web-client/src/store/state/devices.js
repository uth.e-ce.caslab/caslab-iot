import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import {api} from '../../utils';

const DEVICES_PATH = 'devices';

const devicesAdapter = createEntityAdapter({});

export const fetchDevices = createAsyncThunk(
  `${DEVICES_PATH}/get`,
  async (unused, {getState, requestId}) => {
    const {
      devices: {loading, currentRequestId},
      auth: {isAdmin},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    let params;
    if (isAdmin) params = {filter: {include: [{relation: 'user'}]}};
    const {data} = await api.get(DEVICES_PATH, params);
    return {devices: data};
  },
);

export const createDevice = createAsyncThunk(
  `${DEVICES_PATH}/post`,
  async (data, {getState, requestId, rejectWithValue}) => {
    const {loading, currentRequestId} = getState().devices;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      const response = await api.post(DEVICES_PATH, data);
      return {device: response.data};
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  },
);

export const deleteDevice = createAsyncThunk(
  `${DEVICES_PATH}/delete`,
  async (id, {getState, requestId, rejectWithValue}) => {
    const {loading, currentRequestId} = getState().devices;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      await api.del(`${DEVICES_PATH}/${id}`);
      return {id};
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  },
);

const pendingReq = (state, {meta}) => {
  if (state.loading === 'idle') {
    state.loading = 'pending';
    state.currentRequestId = meta.requestId;
  }
};

const rejectedReq = (state, {meta}) => {
  if (
    state.loading === 'pending' &&
    state.currentRequestId === meta.requestId
  ) {
    state.loading = 'idle';
    state.currentRequestId = undefined;
  }
};

export const slice = createSlice({
  name: 'devices',
  initialState: {
    loading: 'idle',
    currentRequestId: undefined,
    ...devicesAdapter.getInitialState(),
  },
  extraReducers: {
    [fetchDevices.pending]: pendingReq,
    [fetchDevices.fulfilled]: (state, {payload, meta}) => {
      if (
        state.loading === 'pending' &&
        state.currentRequestId === meta.requestId
      ) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        devicesAdapter.setAll(state, payload.devices);
      }
    },
    [fetchDevices.rejected]: rejectedReq,
    [createDevice.pending]: pendingReq,
    [createDevice.fulfilled]: (state, {payload, meta}) => {
      if (
        state.loading === 'pending' &&
        state.currentRequestId === meta.requestId
      ) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        devicesAdapter.addOne(state, payload.device);
      }
    },
    [createDevice.rejected]: rejectedReq,
    [deleteDevice.pending]: pendingReq,
    [deleteDevice.fulfilled]: (state, {payload, meta}) => {
      if (
        state.loading === 'pending' &&
        state.currentRequestId === meta.requestId
      ) {
        state.loading = 'idle';
        state.currentRequestId = undefined;
        devicesAdapter.removeOne(state, payload.id);
      }
    },
    [deleteDevice.rejected]: rejectedReq,
  },
});

export default slice.reducer;
