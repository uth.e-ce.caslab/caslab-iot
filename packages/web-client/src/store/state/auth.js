import {createSlice} from '@reduxjs/toolkit';
import {api} from '../../utils';

export const slice = createSlice({
  name: 'auth',
  initialState: {
    authenticated: false,
    token: '',
    id: '',
    isAdmin: false,
    rememberMe: false,
    username: '',
  },
  reducers: {
    login: (state, {payload}) => {
      state.authenticated = true;
      state.token = payload.token;
      state.id = payload.id;
      state.isAdmin = payload.isAdmin;
      state.rememberMe = payload.rememberMe;
      state.username = payload.username;
    },
    logout: state => {
      state.authenticated = false;
      state.token = '';
      state.id = '';
      state.isAdmin = false;
      state.rememberMe = false;
      state.username = '';
    },
  },
});

export const {login, logout} = slice.actions;

async function loginRequest(credentials) {
  const {
    data: {id, token, roles},
  } = await api.post('auth/login', credentials);
  api.setBearerToken(token);
  return {
    id,
    token,
    isAdmin: roles.includes('admin'),
  };
}

async function logoutRequest() {
  // TODO: implement this
  api.setAuthenticationHeader(undefined);
}

export const loginAsync = ({
  rememberMe = false,
  username,
  password,
}) => async dispatch => {
  const {token, id, isAdmin} = await loginRequest({username, password});
  return dispatch(
    login({
      token,
      id,
      isAdmin,
      rememberMe,
      username,
    }),
  );
};

export const logoutAsync = () => async dispatch =>
  logoutRequest().then(() => dispatch(logout()));

export default slice.reducer;
