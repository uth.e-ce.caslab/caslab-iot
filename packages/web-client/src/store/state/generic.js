import {createSlice} from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'generic',
  initialState: {
    isMenuHidden: false,
  },
  reducers: {
    showMenu: state => (state.isMenuHidden = false),
    hideMenu: state => (state.isMenuHidden = true),
  },
});

export const {
  actions: {showMenu, hideMenu},
} = slice;

export default slice.reducer;
