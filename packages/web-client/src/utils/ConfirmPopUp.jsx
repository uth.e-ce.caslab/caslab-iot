import {Box, Button, Heading, Layer, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';

const ConfirmPopUp = ({
  title,
  message,
  confirmColor,
  confirmMessage,
  onClose,
  onConfirm,
}) => {
  return (
    <Layer
      position="center"
      modal={true}
      responsive={false}
      onEsc={onClose}
      onClickOutside={onClose}
    >
      <Box pad="medium" gap="small" width="medium">
        <Heading level={3} margin="none">
          {title}
        </Heading>
        <Text>{message}</Text>
        <Box
          as="footer"
          gap="small"
          direction="row"
          align="center"
          justify="end"
          pad={{top: 'medium', bottom: 'small'}}
        >
          <Button
            label={<strong>Cancel</strong>}
            onClick={onClose}
            color="dark-3"
          />
          <Button
            label={<strong>{confirmMessage}</strong>}
            onClick={onConfirm}
            primary
            color={confirmColor}
          />
        </Box>
      </Box>
    </Layer>
  );
};

ConfirmPopUp.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  confirmBackground: PropTypes.string.isRequired,
  confirmMessage: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};
ConfirmPopUp.defaultProps = {
  title: 'Confirm',
  confirmColor: '',
  confirmMessage: 'Confirm',
};
export default ConfirmPopUp;
