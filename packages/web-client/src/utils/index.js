import api from './api';
export {default as api} from './api';

export const bindFunctions = (self, functions) => {
  functions.forEach(f => {
    self[f] = self[f].bind(self);
  });
};

export function updateItemInArray(array, idProp, idValue, updateItemCallback) {
  return array.map(item => {
    if (item[idProp] !== idValue) return item; // do not touch rest
    return updateItemCallback(item); // transform found item
  });
}

export default {
  bindFunctions,
  updateItemInArray,
  api,
};
