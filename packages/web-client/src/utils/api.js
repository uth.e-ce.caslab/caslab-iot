import axios from 'axios';

export const BASE_URL = '/api/v1';

export const instance = axios.create({
  baseURL: BASE_URL,
});

export const addInterceptor = (type, normalHandler, errorHandler) => {
  if (type !== 'request' && type !== 'response') {
    throw new Error('Wrong interceptor type.');
  }
  return instance.interceptors[type].use(normalHandler, errorHandler);
};

export const setBearerToken = token => {
  instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const setAuthenticationHeader = token => {
  instance.defaults.headers.common['Authorization'] = token;
};

export const post = (url, data) => {
  return instance.post(url, data);
};

export const patch = (url, data) => {
  return instance.patch(url, data);
};

export const get = (url, params) => {
  return instance.get(url, {params});
};

export const del = url => {
  return instance.delete(url);
};

export default {
  BASE_URL,
  instance,
  addInterceptor,
  setBearerToken,
  setAuthenticationHeader,
  post,
  patch,
  get,
  del,
};
