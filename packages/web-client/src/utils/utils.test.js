const {bindFunctions} = require('.');

describe('Utils', () => {
  describe('bindFunctions', () => {
    function incrBy2() {
      return this.prop + 2;
    }
    function decrBy1() {
      return this.prop - 1;
    }
    test('bind a method', () => {
      function incrBy2() {
        return this.prop + 2;
      }
      const x = {
        incrBy2,
        prop: 1,
      };
      bindFunctions(x, ['incrBy2']);
      expect(x.incrBy2()).toBe(3);
    });
    test('bind multiple methods', () => {
      const x = {
        incrBy2,
        decrBy1,
        prop: 1,
      };
      bindFunctions(x, ['incrBy2', 'decrBy1']);
      expect(x.incrBy2()).toBe(3);
      expect(x.decrBy1()).toBe(0);
    });
  });
});
