import {Box, Button, Heading, ResponsiveContext} from 'grommet';
import {Logout, Notification} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {NotificationPopUp} from '../notifications';
import {logoutAsync} from '../store/state/auth';
import {hidePopUp} from '../store/state/notifications';
import Devices from './devices/Devices';
import {AppBar, BottomBar, SideBar} from './nav';
import {Notifications} from './notifications';
import {contentColor} from './style';
import {Users} from './users';

function Dashboard({isAdmin, username, notifications, hidePopUp, logoutAsync}) {
  const [showNotifications, setShowNotifications] = useState(false);
  const size = React.useContext(ResponsiveContext);
  const isMobile = size === 'small';
  const {path} = useRouteMatch();
  // this is done this way to avoid errors when dashboard's root path is '/'
  const rootPath = path === '/' ? '' : path;
  return (
    <Box fill>
      {!notifications.isPopUpHidden && (
        <NotificationPopUp
          status={notifications.popUpStatus}
          message={notifications.popUpMessage}
          hide={hidePopUp}
        />
      )}
      <AppBar>
        <Heading level="3" margin="none">
          CASlab
        </Heading>
        <Box direction="row" margin="none">
          <Button
            icon={<Notification />}
            onClick={() => setShowNotifications(!showNotifications)}
            disabled={true}
          />
          <Button icon={<Logout />} onClick={() => logoutAsync()} />
        </Box>
      </AppBar>
      <Box
        direction="row"
        flex
        overflow={{horizontal: 'hidden'}}
        onMouseUp={() => setShowNotifications(false)}
      >
        {!isMobile && <SideBar rootPath={rootPath} />}
        <Box
          flex
          align="start"
          justify="start"
          background={contentColor}
          pad="xsmall"
        >
          <Switch>
            <Route path={`${rootPath}/dashboard`}>Welcome {username}</Route>
            <Route path={`${rootPath}/devices`} component={Devices} />
            {isAdmin && <Route path={`${rootPath}/users`} component={Users} />}
            <Redirect to={`${rootPath}/dashboard`} />
          </Switch>
        </Box>
        <Notifications show={showNotifications} />
      </Box>
      {isMobile && <BottomBar rootPath={rootPath} />}
    </Box>
  );
}

Dashboard.propTypes = {
  logoutAsync: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  notifications: PropTypes.shape({
    isPopUpHidden: PropTypes.bool.isRequired,
    popUpMessage: PropTypes.string.isRequired,
    popUpStatus: PropTypes.string.isRequired,
  }).isRequired,
  hidePopUp: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isAdmin: state.auth.isAdmin,
  username: state.auth.username,
  notifications: state.notifications,
});

const mapActionsToProps = {logoutAsync, hidePopUp};

export default connect(mapStateToProps, mapActionsToProps)(Dashboard);
