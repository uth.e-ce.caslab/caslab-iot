import {Box, Button, DataTable, Text, TextInput} from 'grommet';
import {Add, Search, Trash} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

function UsersList({users, usersPath}) {
  const [filter, setFilter] = useState('');
  const finalUsers = users.filter(u => u.username.includes(filter));
  let history = useHistory();
  return (
    <Box flex fill gap="xsmall">
      <Box direction="row" gap="between">
        <TextInput
          plain
          icon={<Search />}
          placeholder="Search..."
          value={filter}
          onChange={e => setFilter(e.target.value)}
        />
        <Button
          icon={<Add />}
          focusIndicator={false}
          onClick={() => history.push(`${usersPath}/new`)}
        />
      </Box>
      <Box background="light-1" elevation="small" flex fill pad="xsmall">
        <DataTable
          sortable={true}
          size="medium"
          columns={[
            {
              property: 'username',
              header: <Text>Username</Text>,
              primary: true,
              sortable: true,
              align: 'center',
              render: datum => (
                <Box>
                  <Text truncate={true}>{datum.username}</Text>
                </Box>
              ),
            },
            {
              property: 'id',
              header: 'Actions',
              sortable: false,
              align: 'center',
              render: datum => (
                <Box direction="row" justify="center">
                  <Button
                    icon={<Trash color="status-critical" />}
                    plain={true}
                    focusIndicator={false}
                    onClick={e => {
                      alert(`TODO: delete ${datum.id}`);
                      e.stopPropagation(); // to disable row select
                    }}
                  />
                </Box>
              ),
            },
          ]}
          onClickRow={({datum}) => history.push(`${usersPath}/${datum.id}`)}
          data={finalUsers}
        />
        {!finalUsers.length && (
          <Text margin={{top: 'xsmall'}} textAlign="center">
            No matching users for this filter
          </Text>
        )}
      </Box>
    </Box>
  );
}

UsersList.propTypes = {
  users: PropTypes.array.isRequired,
  usersPath: PropTypes.string.isRequired,
};

export default UsersList;
