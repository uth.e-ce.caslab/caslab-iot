import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {fetchUsers} from '../../store/state/users';
import NewUser from './NewUser';
import User from './User';
import UsersList from './UsersList';

function Users(props) {
  const {fetchUsers} = props;
  const [needFetch, setNeedFetch] = useState(true);
  useEffect(() => {
    fetchUsers();
    setNeedFetch(false);
  }, [fetchUsers]);
  const {path} = useRouteMatch();
  return needFetch || props.loading === 'pending' ? (
    'Loading!'
  ) : (
    <Switch>
      <Route exact path={path}>
        <UsersList users={Object.values(props.users)} usersPath={path} />
      </Route>
      <Route path={`${path}/new`} component={NewUser} />
      <Route
        path={`${path}/:userId`}
        component={() => <User usersPath={path} />}
      />
    </Switch>
  );
}

Users.propTypes = {
  fetchUsers: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  loading: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  users: state.users.entities,
  loading: state.users.loading,
});

export default connect(mapStateToProps, {fetchUsers})(Users);
