import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {Redirect, useParams} from 'react-router-dom';

function User({users, usersPath}) {
  let {userId} = useParams();
  const user = users[userId];
  if (!user) return <Redirect to={usersPath} />;
  return user.username;
}

User.propTypes = {
  users: PropTypes.object.isRequired,
  usersPath: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  users: state.users.entities,
});

export default connect(mapStateToProps)(User);
