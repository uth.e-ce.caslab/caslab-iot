import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';

function DeviceMeta({meta, ...extra}) {
  return (
    <Box direction="row" gap="small" justify="between" {...extra}>
      {meta.lastSeen && (
        <Box direction="column">
          <Text>Last Seen</Text>
          <Text weight="bold">{moment(meta.lastSeen).fromNow()}</Text>
        </Box>
      )}
      {meta.ipv4 && (
        <Box direction="column">
          <Text>IPv4</Text>
          <Text weight="bold">{meta.ipv4}</Text>
        </Box>
      )}
      {meta.ipv6 && (
        <Box direction="column">
          <Text>IPv6</Text>
          <Text>{meta.ipv6}</Text>
        </Box>
      )}
    </Box>
  );
}

DeviceMeta.propTypes = {
  meta: PropTypes.object.isRequired,
};

export default DeviceMeta;
