import {Box, Layer, ResponsiveContext} from 'grommet';
import {LinkPrevious} from 'grommet-icons/index';
import {Button, Heading} from 'grommet/index';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {Redirect, useHistory, useParams} from 'react-router-dom';
import DeviceDetails from './DeviceDetails';
import DeviceMeta from './DeviceMeta';
import DeviceCommands from './DeviceCommands';
import DeviceSensors from './DeviceSensors';
import Breadcrumbs from '../../nav/Breadcrumbs';
import {shortId} from '@caslab-iot/common';
import {Home} from 'grommet-icons';

function Device({devices, devicesPath}) {
  const history = useHistory();
  const {deviceId} = useParams();
  const size = React.useContext(ResponsiveContext);
  const device = devices[deviceId];
  if (!device) return <Redirect to={devicesPath} />;
  const isMobile = size === 'small';
  let prefix;
  if (device.isBorder) {
    prefix = 'Border';
  } else {
    prefix = 'Edge';
  }
  const title = (
    <Box direction="row" fill="horizontal" justify="center">
      <Heading level={2} margin="none" alignSelf="center">
        {prefix} Device
      </Heading>
    </Box>
  );
  return isMobile ? (
    <Layer>
      <Box fill="vertical">
        <Box flex={false} direction="row" justify="between" elevation="small">
          <Button
            icon={<LinkPrevious />}
            onClick={() => history.push(devicesPath)}
          />
          {title}
          <Button
            icon={<LinkPrevious />}
            style={{visibility: 'hidden'}}
            disabled={true}
          />
        </Box>
        <Box fill direction="column" gap="xsmall">
          <Box elevation="small" pad="small" gap="medium">
            <DeviceDetails device={device} />
            {!!Object.keys(device.meta).length && (
              <DeviceMeta meta={device.meta} />
            )}
          </Box>
          <DeviceCommands device={device} />
          <DeviceSensors device={device} />
        </Box>
      </Box>
    </Layer>
  ) : (
    <Box fill gap="xsmall">
      <Breadcrumbs
        steps={[
          {label: '', Icon: Home, path: '/'},
          {label: 'Devices', path: devicesPath},
          {label: device.name ?? shortId(device.id), path: ''},
        ]}
      />
      {title}
      <Box fill="horizontal" direction="row" gap="xsmall">
        <Box
          width="medium"
          flex="grow"
          elevation="small"
          pad="small"
          gap="medium"
        >
          <DeviceDetails device={device} />
          {!!Object.keys(device.meta).length && (
            <DeviceMeta meta={device.meta} />
          )}
        </Box>
        <Box width="medium" flex="grow">
          <DeviceCommands device={device} />
          <DeviceSensors device={device} />
        </Box>
      </Box>
    </Box>
  );
}

Device.propTypes = {
  devices: PropTypes.object.isRequired,
  devicesPath: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  devices: state.devices.entities,
});

export default connect(mapStateToProps)(Device);
