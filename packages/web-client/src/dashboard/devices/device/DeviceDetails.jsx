import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {shortId} from '@caslab-iot/common';

function DeviceDetails({device, ...extra}) {
  return (
    <Box gap="medium" fill="horizontal" {...extra}>
      <Box border="bottom" pad={{bottom: 'small'}}>
        <Text>Name</Text>
        <Text weight="bold">{device.name}</Text>
      </Box>
      <Box direction="row" gap="small" justify="between">
        <Box direction="column">
          <Text>Status</Text>
          <Text weight="bold">{device.status ? 'Online' : 'Offline'}</Text>
        </Box>
        <Box direction="column">
          <Text>UUID</Text>
          <Text weight="bold">{shortId(device.id)}</Text>
        </Box>
        <Box direction="column">
          <Text>Type</Text>
          <Text weight="bold">{device.type}</Text>
        </Box>
      </Box>
    </Box>
  );
}

DeviceDetails.propTypes = {
  device: PropTypes.object.isRequired,
};

export default DeviceDetails;
