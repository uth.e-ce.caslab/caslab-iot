import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {shortId} from '@caslab-iot/common';

function DeviceSensors({device}) {
  // TODO: implement this
  return (
    <Box elevation="small" pad="small" gap="medium" fill="horizontal">
      <Text>Sensors of device {device.name ?? shortId(device.id)}</Text>
    </Box>
  );
}

DeviceSensors.propTypes = {
  device: PropTypes.object.isRequired,
};

export default DeviceSensors;
