import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {shortId} from '@caslab-iot/common';

function DeviceCommands({device}) {
  // TODO: implement this
  return (
    <Box elevation="small" pad="small" gap="medium" fill="horizontal">
      <Text>Commands for device {device.name ?? shortId(device.id)}</Text>
    </Box>
  );
}

DeviceCommands.propTypes = {
  device: PropTypes.object.isRequired,
};

export default DeviceCommands;
