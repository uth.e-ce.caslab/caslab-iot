import {BorderTypes, EdgeTypes} from '@caslab-iot/common';
import {unwrapResult} from '@reduxjs/toolkit';
import {
  Box,
  Button,
  Form,
  FormField,
  Heading,
  Layer,
  ResponsiveContext,
  Select,
  TextInput,
} from 'grommet';
import {Close, LinkPrevious} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {createDevice} from '../../store/state/devices';
import {showPopUp} from '../../store/state/notifications';

function NewDevice({onExit, createDevice, showPopUp, isBorder}) {
  const [errors, setErrors] = useState({});
  const size = React.useContext(ResponsiveContext);
  const isMobile = size === 'small';
  let title;
  let devTypes;
  if (isBorder) {
    title = 'Border';
    devTypes = Object.values(BorderTypes);
  } else {
    title = 'Edge';
    devTypes = Object.values(EdgeTypes);
  }
  const titleEl = (
    <Heading level={2} margin="none" alignSelf="center">
      Add {title} Device
    </Heading>
  );
  return (
    <Layer
      onEsc={onExit}
      onClickOutside={onExit}
      position="right"
      full="vertical"
    >
      <Form
        errors={errors}
        onSubmit={async ({value}) => {
          const action = await createDevice({
            ...value,
            isBorder,
          });
          try {
            unwrapResult(action);
            onExit();
          } catch (unused) {
            const {details, message} = action.payload?.error || {};
            if (details) {
              const newErrors = details.reduce((o, {path, message}) => {
                const propName = path.replace('/', '');
                o[propName] = message;
                return o;
              }, {});
              setErrors(newErrors);
            } else if (message) {
              setErrors({});
              showPopUp({message, status: 'critical'});
            }
          }
        }}
        style={{height: '100%'}}
      >
        <Box fill="vertical">
          {isMobile ? (
            <Box
              flex={false}
              direction="row"
              justify="between"
              elevation="small"
            >
              <Button icon={<LinkPrevious />} onClick={onExit} />
              <Box direction="row" fill="horizontal" justify="center">
                {titleEl}
              </Box>
              <Button
                icon={<LinkPrevious />}
                style={{visibility: 'hidden'}}
                disabled={true}
              />
            </Box>
          ) : (
            <Box
              flex={false}
              direction="row"
              justify="between"
              pad={{vertical: 'small', left: 'small', right: 'xsmall'}}
            >
              {titleEl}
              <Button icon={<Close />} onClick={onExit} />
            </Box>
          )}
          <Box
            pad="medium"
            justify={isMobile ? 'between' : 'start'}
            fill={isMobile ? 'vertical' : false}
          >
            <Box flex="grow">
              <FormField name="name" label="Name">
                <TextInput name="name" />
              </FormField>
              <FormField name="type" label="Type" required>
                <Select name="type" options={devTypes} />
              </FormField>
            </Box>
            <Box flex={false} align="start" margin={{top: 'small'}}>
              <Button type="submit" label="Submit" primary />
            </Box>
          </Box>
        </Box>
      </Form>
    </Layer>
  );
}

NewDevice.propTypes = {
  onExit: PropTypes.func.isRequired,
  createDevice: PropTypes.func.isRequired,
  showPopUp: PropTypes.func.isRequired,
  isBorder: PropTypes.bool.isRequired,
};

const mapActionsToProps = {createDevice, showPopUp};

export default connect(null, mapActionsToProps)(NewDevice);
