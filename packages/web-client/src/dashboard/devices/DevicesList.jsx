import {unwrapResult} from '@reduxjs/toolkit';
import {Box, Button, DataTable, Select, Text, TextInput} from 'grommet';
import {Add, Search, Trash} from 'grommet-icons';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {deleteDevice} from '../../store/state/devices';
import {showPopUp} from '../../store/state/notifications';
import ConfirmPopUp from '../../utils/ConfirmPopUp';
import NewDevice from './NewDevice';
import {shortId} from '@caslab-iot/common';

const searchFields = ['id', 'name', 'type', 'user.username'];
const allColumns = [
  {
    property: 'id',
    header: <Text>UUID</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box alignSelf="center">
        <Box width={{max: '100px'}} alignSelf="center">
          <Text truncate={true} textAlign="center">
            {shortId(datum.id)}
          </Text>
        </Box>
      </Box>
    ),
  },
  {
    property: 'name',
    header: <Text>Name</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box>
        <Text truncate={true}>{datum.name}</Text>
      </Box>
    ),
  },
  {
    property: 'type',
    header: <Text>Type</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box>
        <Text truncate={true}>{datum.type}</Text>
      </Box>
    ),
  },
  {
    property: 'userId',
    header: <Text>User</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box>
        <Text truncate={true}>{datum.user?.username}</Text>
      </Box>
    ),
  },
];
const isBorderOpts = [
  {label: 'Border', value: true},
  {label: 'Edge', value: false},
];

function DevicesList({devices, devicesPath, isAdmin, deleteDevice, showPopUp}) {
  const history = useHistory();
  const [filters, setFilters] = useState({
    search: '',
    isBorder: isBorderOpts[0],
  });
  const [filteredDevices, setFilteredDevices] = useState(devices);
  useEffect(() => {
    setFilteredDevices(
      devices.filter(
        dev =>
          dev.isBorder === filters.isBorder.value &&
          searchFields.some(
            f =>
              _.has(dev, f) &&
              _.get(dev, f)
                .toLowerCase()
                .includes(filters.search.toLowerCase()),
          ),
      ),
    );
  }, [devices, filters]);
  const [showNew, setShowNew] = useState(false);
  const [deleteDialogId, setDeleteDialogId] = useState('');
  const [columns, setColumns] = useState(allColumns);
  useEffect(() => {
    if (!isAdmin) {
      setColumns(allColumns.filter(({property}) => property !== 'userId'));
    }
  }, [isAdmin]);
  return (
    <Box flex fill gap="xsmall">
      <Box direction="row" gap="between">
        <Select
          value={filters.isBorder}
          labelKey="label"
          valueKey="value"
          options={isBorderOpts}
          onChange={({value}) => setFilters({...filters, isBorder: value})}
        />
        <Box direction="row" fill="horizontal">
          <TextInput
            plain
            icon={<Search />}
            placeholder="Search..."
            value={filters.search}
            onChange={({target: {value}}) =>
              setFilters({
                ...filters,
                search: value,
              })
            }
            style={{height: '100%'}}
          />
        </Box>
        <Button
          icon={<Add />}
          focusIndicator={false}
          onClick={() => setShowNew(true)}
        />
      </Box>
      <Box
        background="light-1"
        elevation="small"
        flex
        fill
        pad="xsmall"
        overflow="scroll"
      >
        <DataTable
          sortable={true}
          columns={[
            ...columns,
            {
              property: 'actions',
              header: 'Actions',
              sortable: false,
              align: 'center',
              render: datum => (
                <Box direction="row" justify="center">
                  <Button
                    icon={<Trash color="status-critical" />}
                    plain={true}
                    focusIndicator={false}
                    onClick={e => {
                      e.stopPropagation(); // to disable row select
                      setDeleteDialogId(datum.id);
                    }}
                  />
                </Box>
              ),
            },
          ]}
          onClickRow={({datum}) => history.push(`${devicesPath}/${datum.id}`)}
          data={filteredDevices}
        />
        {(!devices.length && (
          <Text margin={{top: 'xsmall'}} textAlign="center">
            No devices
          </Text>
        )) ||
          (!filteredDevices.length && (
            <Text margin={{top: 'xsmall'}} textAlign="center">
              No matching devices for this filter
            </Text>
          ))}
        {deleteDialogId && (
          <ConfirmPopUp
            message="Are you sure you want to delete?"
            confirmMessage="Delete"
            confirmColor="status-critical"
            onClose={() => setDeleteDialogId('')}
            onConfirm={async () => {
              setDeleteDialogId('');
              const action = await deleteDevice(deleteDialogId);
              try {
                unwrapResult(action);
                showPopUp({
                  message: 'Deleted device successfully',
                  status: 'ok',
                });
              } catch (unused) {
                showPopUp({
                  message: 'Delete device failed',
                  status: 'critical',
                });
              }
            }}
          />
        )}
        {showNew && (
          <NewDevice
            isBorder={filters.isBorder.value}
            onExit={() => setShowNew(false)}
          />
        )}
      </Box>
    </Box>
  );
}

DevicesList.propTypes = {
  devices: PropTypes.array.isRequired,
  devicesPath: PropTypes.string.isRequired,
  isAdmin: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isAdmin: state.auth.isAdmin,
});

const mapActionsToProps = {deleteDevice, showPopUp};

export default connect(mapStateToProps, mapActionsToProps)(DevicesList);
