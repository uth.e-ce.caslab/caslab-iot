import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {fetchDevices} from '../../store/state/devices';
import Device from './device/Device';
import DevicesList from './DevicesList';

function Devices(props) {
  const {fetchDevices} = props;
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    fetchDevices().then(() => setLoading(false));
  }, [fetchDevices]);
  const {path} = useRouteMatch();
  return loading ? (
    'Loading!'
  ) : (
    <Switch>
      <Route exact path={path}>
        <DevicesList
          devices={Object.values(props.devices)}
          devicesPath={path}
        />
      </Route>
      <Route
        path={`${path}/:deviceId`}
        component={() => <Device devicesPath={path} />}
      />
    </Switch>
  );
}

Devices.propTypes = {
  fetchDevices: PropTypes.func.isRequired,
  devices: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  devices: state.devices.entities,
});

export default connect(mapStateToProps, {fetchDevices})(Devices);
