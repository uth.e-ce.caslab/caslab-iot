import {Dashboard, Device, Group} from 'grommet-icons';

export default [
  {label: 'Dashboard', Icon: Dashboard, path: '/dashboard', adminOnly: false},
  {label: 'Devices', Icon: Device, path: '/devices', adminOnly: false},
  {label: 'Users', Icon: Group, path: '/users', adminOnly: true},
];
