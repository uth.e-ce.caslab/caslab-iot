import PropTypes from 'prop-types';
import React from 'react';
import NavBar from './NavBar';

const SideBar = ({rootPath}) => {
  return (
    <NavBar
      rootPath={rootPath}
      wrapperProps={{justify: 'start'}}
      elemProps={{gap: 'xsmall', pad: 'medium'}}
      iconStyle={{size: 'large'}}
    />
  );
};
SideBar.propTypes = {
  rootPath: PropTypes.string.isRequired,
};

export default SideBar;
