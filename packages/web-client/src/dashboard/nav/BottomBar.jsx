import PropTypes from 'prop-types';
import React from 'react';
import NavBar from './NavBar';

const BottomBar = ({rootPath}) => {
  return (
    <NavBar
      rootPath={rootPath}
      wrapperProps={{justify: 'around', direction: 'row'}}
      elemProps={{pad: 'xsmall'}}
      activeElemProps={{gap: 'xsmall'}}
    />
  );
};
BottomBar.propTypes = {
  rootPath: PropTypes.string.isRequired,
};

export default BottomBar;
