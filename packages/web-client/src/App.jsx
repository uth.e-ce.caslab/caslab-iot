import {ConnectedRouter} from 'connected-react-router';
import {Grommet} from 'grommet';
import React from 'react';
import {Provider} from 'react-redux';
import {Redirect, Switch} from 'react-router-dom';
import {Login, loginRoute, PrivateRoute, PublicOnlyRoute} from './auth';
import {Dashboard} from './dashboard';
import configureStore, {history} from './store/configureStore';

const theme = {
  global: {
    colors: {
      brand: '#00458E',
      focus: 'brand',
    },
    font: {
      family: 'Roboto',
    },
  },
};

export const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <Grommet theme={theme} full>
        <ConnectedRouter history={history}>
          <Switch>
            <PublicOnlyRoute path={loginRoute}>
              <Login />
            </PublicOnlyRoute>
            <PrivateRoute path="/">
              <Dashboard />
            </PrivateRoute>
            <Redirect to="/" />
          </Switch>
        </ConnectedRouter>
      </Grommet>
    </Provider>
  );
}

export default App;
