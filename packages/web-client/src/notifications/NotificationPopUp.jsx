import {Box, Button, Layer, ResponsiveContext, Text} from 'grommet';
import {
  FormClose,
  StatusCritical,
  StatusGood,
  StatusUnknown,
  StatusWarning,
} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useEffect} from 'react';

const statusMap = {
  ok: {
    background: 'status-ok',
    Icon: StatusGood,
  },
  warning: {
    background: 'status-warning',
    Icon: StatusWarning,
  },
  critical: {
    background: 'status-critical',
    Icon: StatusCritical,
  },
  unknown: {
    background: 'status-unknown',
    Icon: StatusUnknown,
  },
};

const NotificationPopUp = ({status, message, hide, hideAfter}) => {
  const {background, Icon} = statusMap.hasOwnProperty(status)
    ? statusMap[status]
    : statusMap.unknown;
  useEffect(() => {
    const autoHideTimer = setTimeout(hide, hideAfter);
    return () => {
      clearTimeout(autoHideTimer);
    };
  });
  const size = React.useContext(ResponsiveContext);
  const isMobile = size === 'small';
  let full;
  let width;
  if (isMobile) {
    full = 'horizontal';
  } else {
    width = {min: 'medium'};
  }
  return (
    <Layer
      position="top"
      modal={false}
      margin="small"
      responsive={false}
      plain
      full={full}
    >
      <Box
        align="center"
        direction="row"
        gap="small"
        justify="between"
        round="medium"
        elevation="medium"
        pad={{vertical: 'xsmall', horizontal: 'small'}}
        background={background}
        width={width}
      >
        <Icon />
        <Text>{message}</Text>
        <Button
          icon={<FormClose />}
          onClick={hide}
          plain
          focusIndicator={false}
        />
      </Box>
    </Layer>
  );
};

NotificationPopUp.propTypes = {
  status: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  hide: PropTypes.func.isRequired,
  hideAfter: PropTypes.number.isRequired,
};
NotificationPopUp.defaultProps = {
  hideAfter: 4000, // ms
};
export default NotificationPopUp;
