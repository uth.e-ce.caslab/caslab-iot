// contains the auth configuration options

export const loginRoute = '/login',
  registerRoute = '/register',
  routeAfterAuth = '/dashboard',
  routeAfterUnauth = loginRoute;

export default {
  routeAfterAuth,
  routeAfterUnauth,
  loginRoute,
  registerRoute,
};
