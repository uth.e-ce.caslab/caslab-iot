import config from './config';
import Login from './Login';
import PrivateRoute from './PrivateRoute';
import PublicOnlyRoute from './PublicOnlyRoute';
export * from './config';
export {PrivateRoute, PublicOnlyRoute, Login};

export default {...config, PrivateRoute, PublicOnlyRoute, Login};
