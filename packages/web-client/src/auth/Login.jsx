import {
  Box,
  Button,
  Form,
  FormField,
  grommet,
  Heading,
  TextInput,
  ThemeContext,
} from 'grommet';
import {Hide, Login as LoginIcon, View} from 'grommet-icons';
import {deepMerge} from 'grommet/utils';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {NotificationPopUp} from '../notifications';
import {loginAsync} from '../store/state/auth';

const formTheme = deepMerge(grommet, {
  formField: {
    border: {position: 'inner', side: 'all'},
  },
});

const Login = props => {
  const [showPass, setShowPass] = useState(false);
  const [error, setError] = useState(null);
  let passType;
  let ShowPassIcon;
  if (showPass) {
    passType = 'text';
    ShowPassIcon = Hide;
  } else {
    passType = 'password';
    ShowPassIcon = View;
  }
  return (
    <Box direction="row" justify="center" background="light-2" fill>
      <Box basis="medium" pad="medium">
        <ThemeContext.Extend value={formTheme}>
          {error && (
            <NotificationPopUp
              status="critical"
              message="Sign in failed!"
              hide={() => setError(null)}
            />
          )}
          <Box direction="row" justify="center">
            <Heading color="brand">CAS</Heading>
            <Heading color="dark-4">lab</Heading>
          </Box>
          <Form
            onSubmit={({value}) => {
              setError(null);
              props.loginAsync({...value}).catch(err => setError(err));
            }}
          >
            <FormField name="username" label="Username" required>
              <TextInput name="username" />
            </FormField>
            <FormField name="password" label="Password" required>
              <Box direction="row" align="center">
                <TextInput name="password" type={passType} plain />
                <Button
                  icon={<ShowPassIcon size="medium" />}
                  onClick={() => setShowPass(!showPass)}
                  focusIndicator={false}
                />
              </Box>
            </FormField>
            <Button
              margin={{top: 'small'}}
              type="submit"
              primary
              label="Sign in"
              fill="horizontal"
              icon={<LoginIcon />}
            />
          </Form>
        </ThemeContext.Extend>
      </Box>
    </Box>
  );
};
Login.propTypes = {
  loginAsync: PropTypes.func.isRequired,
};
export default connect(null, {loginAsync})(Login);
