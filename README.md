# CASlab IoT

This repo will host all [CASlab](https://caslab.e-ce.uth.gr/) IoT projects.

### Contribute

- [Monorepo Overview](./docs/MONOREPO.md)
- [Contributing Guidelines](CONTRIBUTING.md)
- [Development Environment](DEVELOP.md)

License: undefined (TODO: [choose license](https://choosealicense.com/))
